FIP Gauges Collection for Flight Simulator
==========================================

Randolf's collection of FIP Gauges for Flight Simulation. Made to work with
[Lorby's Axis And Ohs][aao] software, compatibility with other software
possible.

[aao]: https://www.axisandohs.com/

| Gauge               | File                        | Example                                    |
| ------------------- | --------------------------- | ------------------------------------------ |
| R EGT               | REngineGauges/REGT.xml      | ![R EGT gauge](REngineGauges/REGT.png)     |
| R OAT               | RGauges/R-OAT.xml           | ![R OAT gauge](RGauges/R-OAT.png)          |
| R Drift Meter HiRes | RGauges/R-DriftMeter-HR.xml | ![R Drift Meter](RGauges/R-DriftMeter.png) |
| R Handheld Radio    | RRadios/R-Radio.xml         | ![R Radio](RRadios/R-Radio.png)            |
| R KX-165A           | RRadios/R-KX-165A.xml       | ![R Radio](RRadios/R-KX-165A.png)          |
| R Chrono            | RGauges/R-Chrono.xml        | ![R Radio](RGauges/R-Chrono.png)           |

Most gauges come in two sizes (resolutions), normal size 240x240 and high
resolution 512x512.


Links
-----

* [Project repository][git]
* [Download ZIP archive][zip]

[git]: https://gitlab.com/randolf.lemmont/fs-fip-gauges
[zip]: https://gitlab.com/randolf.lemmont/fs-fip-gauges/-/archive/master/fs-fip-gauges-master.zip


Install
-------

Place folders from this repository (archive) into the directory where your
software expects FIP gauge files, for example `Documents\LorbyAxisAndOhs
Files\UserGauges\RGauges` and install the DSEG font family as described in the
next section.


Fonts
-----

For LCD numbers on some gauges, please download and install [DSEG font
family][dseg] by Keshikan. Fonts used are also included here in fonts directory
for archival and convenience.

[dseg]: https://www.keshikan.net/fonts-e.html
